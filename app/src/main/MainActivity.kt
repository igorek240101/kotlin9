package com.example.kotlin9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var imageView: ImageView
    private lateinit var button: Button

    private val imageList = arrayOf(R.drawable.image1, R.drawable.image2, R.drawable.image3)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.imageView)
        button = findViewById(R.id.button)

        button.setOnClickListener {
            val random = Random()
            val image = imageList[random.nextInt(imageList.size)]
            imageView.setImageResource(image)

// изменение цвета кнопки
            val color = getRandomColor()
            button.setBackgroundColor(color)
        }
    }

    private fun getRandomColor(): Int {
        val random = Random()
        val r = random.nextInt(256)
        val g = random.nextInt(256)
        val b = random.nextInt(256)
        return android.graphics.Color.rgb(r, g, b)
    }
}